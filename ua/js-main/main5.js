$(function () {
    const windowWidth = screen.width;


    $('#hamburger').on('click', function () {
        $(this).toggleClass('open');
        $('.navigation nav').toggleClass('open');
        $('.header-contact').removeClass('open');
        $('.mobile-contact-btn').removeClass('open');
        $('.js-lang-menu').removeClass('open');
    });

    $('.js-mobile-contact-btn').on('click', function () {
        $(this).toggleClass('open');
        $('.header-contact').toggleClass('open');
        $('.navigation nav').removeClass('open');
        $('#hamburger').removeClass('open');
        $('.js-lang-menu').removeClass('open');
        $('.js-lang').removeClass('open');
    });

    $('.js-lang').on('click',function(){
        $(this).toggleClass('open');
        $('.js-lang-menu').toggleClass('open');
        $('.js-mobile-contact-btn').removeClass('open');
        $('.header-contact').removeClass('open');
        $('.navigation nav').removeClass('open');
        $('#hamburger').removeClass('open');
    })

    $('.dropdown').on('click', function() {
        $(this).find('.dropdown-list').toggleClass('d-none');
    });

    if(screen.width > 1061) {
        $(document).mouseup(function (e){
            var div = $(".dropdown-list");
            if (!div.is(e.target)
                && div.has(e.target).length === 0) {
                div.addClass('d-none');
            }
        });
    }
});


