$(function () {
    var $grid = $('.blog-elems').isotope({
        itemSelector: '.blog-elem',
        layoutMode: 'fitRows'
    });

    $('.filter__item').on('click', function () {
        $('.filter__item').removeClass('active');
        $(this).addClass('active');
        var filterValue = $(this).attr('data-filter');
        $grid.isotope({ filter: filterValue });
    });
});