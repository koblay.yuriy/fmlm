$(function () {
    $('.review-elem__text p a').on('click', function () {
        const photo = $(this).parent().parent().siblings('.review-elem__user').find('.review-photo-js');
        const media = $(this).parent().parent().siblings('.review-elem__media-box').find('.review-preview-js');
        const mediaData = $(this).parent().parent().siblings('.review-elem__media-box').find('button');
        const audio = $(this).parent().parent().siblings('.review-elem__media-box').find('source');
        const facebook = $(this).parent().parent().siblings('.review-elem__user').find('.facebook');
        const instagram = $(this).parent().parent().siblings('.review-elem__user').find('.instagram');
        const vk = $(this).parent().parent().siblings('.review-elem__user').find('.vk');
        const site = $(this).parent().parent().siblings('.review-elem__user').find('.site');

        $('.reviewDetail-photo').attr('src', photo.attr('src'));
        $('.reviewDetail-photo').attr('alt', photo.attr('alt'));
        $('.reviewDetail-name').text($(this).parent().parent().siblings('.review-elem__user').find('.review-name-js').text());
        $('.reviewDetail-company').text($(this).parent().parent().siblings('.review-elem__user').find('.review-company-js').text());
        $('.reviewDetail-text').text($(this).parent().siblings('.review-text-js').text());
        if (media.attr('src')) {
            $('.reviewDetail-preview').attr('src', media.attr('src'));
            $('.reviewDetail-preview').attr('alt', media.attr('alt'));
            $('.reviewDetail-media button').attr('data-fancybox', mediaData.attr('data-fancybox'));
            $('.reviewDetail-media button').attr('data-src', mediaData.attr('data-src'));
            $('.reviewDetail-media').removeClass('d-none').addClass('d-flex');
        } else {
            $('.reviewDetail-media').addClass('d-none').removeClass('d-flex');
        }
        if (audio.attr('src')) {
            $('.reviewDetail-media-box audio').attr('src', audio.attr('src'));
            $('.reviewDetail-media-box audio').removeClass('d-none');
        }

        if (facebook.attr('href')) {
            $('.reviewDetail-social .facebook').removeClass('d-none').attr('href', facebook.attr('href')).addClass('d-flex')
            $('.reviewDetail-social .facebook span').text(facebook.find('span').text());
        }
        if (instagram.attr('href')) {
            $('.reviewDetail-social .instagram').removeClass('d-none').attr('href', instagram.attr('href')).addClass('d-flex')
            $('.reviewDetail-social .instagram span').text(instagram.find('span').text());
        }
        if (vk.attr('href')) {
            $('.reviewDetail-social .vk').removeClass('d-none').attr('href', vk.attr('href')).addClass('d-flex')
            $('.reviewDetail-social .vk span').text(vk.find('span').text());
        }
        if (site.attr('href')) {
            $('.reviewDetail-social .site').removeClass('d-none').attr('href', site.attr('href')).addClass('d-flex')
            $('.reviewDetail-social .site span').text(site.find('span').text());
        }
    });

    Fancybox.bind("[data-fancybox]", {
        on: {
            closing: (fancybox, slide) => {
                $('.reviewDetail-photo').attr('src', '');
                $('.reviewDetail-photo').attr('alt', '');
                $('.reviewDetail-name').text('');
                $('.reviewDetail-company').text('');
                $('.reviewDetail-text').text('');
                $('.reviewDetail-preview').attr('src', '');
                $('.reviewDetail-preview').attr('alt', '');
                $('.reviewDetail-media').addClass('d-none');
                $('.reviewDetail-media-box audio').attr('src', '');
                $('.reviewDetail-media-box audio').addClass('d-none');
                $('.reviewDetail-social a').attr('href', '').addClass('d-none').removeClass('d-flex');
            },
        }
    });
})