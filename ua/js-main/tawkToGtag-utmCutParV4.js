function getUtm(){
	/* get utm from url to local storage if not exist */
	const queryString = window.location.href.split('?')[1];
	if (!!queryString) {
		const utmStr = queryString.replace(/%/g, ''); // cut all % symbol
	    !localStorage.getItem('utm_labels') ? localStorage.setItem('utm_labels', utmStr) : null;
	}	
	/* parsing utm from local storage to objects */
    const utmObject = {};
    const utmPairs = !!localStorage.getItem('utm_labels') ? localStorage.getItem('utm_labels').split('&') : [];
    for (let i = 0; i < utmPairs.length; i++) {
        const pair = utmPairs[i].split('=');
        const key = pair[0];
        const value = pair[1];
        utmObject[key] = value;
    }
    return utmObject;	
}
var utm = getUtm();

var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
Tawk_API.visitor = {
    email : utm['utm_campaign']+'.'+utm['utm_content']+'@email.com'
};

/* for gtag.js with ***** */
Tawk_API.onChatStarted = function(){
	gtag('event', 'chat_started', {
		'event_category': 'tawk.to',
	});
}; 
Tawk_API.onChatEnded = function(){
	gtag('event', 'chat_ended', {
		'event_category': 'tawk.to',
	});
};
Tawk_API.onPrechatSubmit = function(data){
	gtag('event', 'prechat_submit', {
		'event_category': 'tawk.to',
	});
	Tawk_API.setAttributes({
		'email' : utm['utm_campaign']+'.'+utm['utm_content']+'@email.com',
		/* 'utm': localStorage.getItem('utm_labels') || 'no-utm-label', */
		'source': utm['utm_source'] || 'no-utm-source',
        'medium': utm['utm_medium'] || 'no-utm-medium',
        'campaign': utm['utm_campaign'] || 'no-utm-campaign',
        'content': utm['utm_content'] || 'no-utm-content'
     /* 'term': utm['utm_term'] || 'no-utm-term hex',
        'network': utm['network'] || 'no-utm-network',
        'source': utm['source'] || 'no-utm-source',
        'position': utm['position'] || 'no-utm-position',
        'match': utm['match'] || 'no-utm-match',
        'gad':utm['gad'] || 'no-utm-gad',
		'gclid':utm['gclid'] || 'no-utm-gclid' */
	}, function(error){});
	Tawk_API.addEvent('set-utm-tags', {
        'source': utm['utm_source'] || 'no-utm-source',
        'medium': utm['utm_medium'] || 'no-utm-medium',
        'campaign': utm['utm_campaign'] || 'no-utm-campaign',
        'content': utm['utm_content'] || 'no-utm-content',
		'term': utm['utm_term'] || 'no-utm-term-hex'
    }, function(error){});
};
Tawk_API.onOfflineSubmit = function(data){
	gtag('event', 'offline_submit', {
		'event_category': 'tawk.to',
	});
};

(function(){
	var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
	s1.async=true;
	s1.src='https://embed.tawk.to/63ee1458c2f1ac1e2033a088/1gpcvuus4';
	s1.charset='UTF-8';
	s1.setAttribute('crossorigin','*');
	s0.parentNode.insertBefore(s1,s0);
})();