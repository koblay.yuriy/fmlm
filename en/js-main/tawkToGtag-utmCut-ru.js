/* get utm from url with cut all % symbol */
function getUtm(){
	const queryString = window.location.href.split('?')[1];
	if (!!queryString) {
		/* cut all % symbol */
		const utmStr = queryString.replace(/%/g, '');
	    !localStorage.getItem('utm_labels') ? localStorage.setItem('utm_labels', utmStr) : null;
	}
}
getUtm()

var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();

/* for gtag.js with ***** */
Tawk_API.onChatStarted = function(){
	gtag('event', 'chat_started', {
		'event_category': 'tawk.to',
	});
}; 
Tawk_API.onChatEnded = function(){
	gtag('event', 'chat_ended', {
		'event_category': 'tawk.to',
	});
};
Tawk_API.onPrechatSubmit = function(data){
	gtag('event', 'prechat_submit', {
		'event_category': 'tawk.to',
	});
	Tawk_API.setAttributes({
		'utm': localStorage.getItem('utm_labels') || 'no utm label'
	}, function(error){});
};
Tawk_API.onOfflineSubmit = function(data){
	gtag('event', 'offline_submit', {
		'event_category': 'tawk.to',
	});
};

(function(){
	var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
	s1.async=true;
	s1.src='https://embed.tawk.to/63ee1458c2f1ac1e2033a088/1h32j688a';
	s1.charset='UTF-8';
	s1.setAttribute('crossorigin','*');
	s0.parentNode.insertBefore(s1,s0);
})();