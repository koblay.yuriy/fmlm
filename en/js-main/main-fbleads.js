$(function () {
    const windowWidth = screen.width;


    $('#hamburger').on('click', function () {
        $(this).toggleClass('open');
        $('.navigation nav').toggleClass('open');
        $('.header-contact').removeClass('open');
        $('.mobile-contact-btn').removeClass('open');
    });

    $('.mobile-contact-btn').on('click', function () {
        $(this).toggleClass('open');
        $('.header-contact').toggleClass('open');
        $('.navigation nav').removeClass('open');
        $('#hamburger').removeClass('open');
    });

    $('.dropdown').on('click', function() {
        $(this).find('.dropdown-list').toggleClass('d-none');
    });

    if(screen.width > 1061) {
        $(document).mouseup(function (e){
            var div = $(".dropdown-list");
            if (!div.is(e.target)
                && div.has(e.target).length === 0) {
                div.addClass('d-none');
            }
        });
    }

    //modal ==============================
    $('[data-toggle="modal"]').on('click',function(){
        var modalID = $(this).attr('href');
        $('.main').find('#' + modalID.slice(1)).addClass('active');
        $('html').addClass('modal--active');
    })
    $('[data-dismiss="modal"]').on('click',function () {
        $(this).closest('.modal').removeClass('active');
        $('html').removeClass('modal--active');
    });
    jQuery(document).mouseup(function (e){
        var div = jQuery(".modal-dialog");
        if (!div.is(e.target)
            && div.has(e.target).length === 0) {
            jQuery(".modal-dialog").parent().removeClass('active');
            event.stopPropagation();
            $("html").removeClass('modal--active');
        }
    });

    /*  main page form
    ===================================     */
    function getToken() {
        $.ajax({
            url: 'https://process.flawless.group/api/',
            method: 'get',
            success: function (response) {
                $('#form-token').val(response.token);
            }
        });
    }

    function sendData(data) {
        $.ajax({
            url: 'https://process.flawless.group/api/contact',
            method: 'post',
            data: {
                url: window.location.href,
                token: $('#form-token').val(),
                name: $('#userName').val(),
                phone: $('#userTel').val()
            },
            success: function (response) {
                if(response.success){
                    $('#modalForm').find('.form__wrapper').html('<div class="text-14 mb-12 text-center text-white">' + response.message + '</div>');
                    window.location.replace('//' + window.location.host + '/fbthanks');
                }else{
                    $('#modalForm').find('.alert-error').removeClass('d-none').html(response.message);
                }
            },
            error : function () {
                $('#modalForm').find('.alert-error').removeClass('d-none').html('Ошибка запроса');
            },
        });
    }

    if($('#callback-form').length) {
        getToken();

        $('#callback-form').on('submit', function (e) {
            e.preventDefault();
            $('#modalForm').find('.alert-error').addClass('d-none').html('');
            sendData();
        });
    }

});


