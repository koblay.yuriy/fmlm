/*
Tawk.to:
<script src="js-main/tawkToGtag.js" async></script>
HubSpot:
<script type="text/javascript" id="hs-script-loader" async defer src="//js-eu1.hs-scripts.com/26919381.js"></script>
Jivo:
<script src="//code-ya.jivosite.com/widget/tdn0fkHPpk" async></script>
*/
const J_PATH = '//code-ya.jivosite.com/widget/tdn0fkHPpk'
const H_PATH = 'js-main/tawkToGtag.js'
let chatLoad = null
let chatType = null
const loadScript = (path) => {
	let script = document.createElement("script")
	script.type = "text/javascript";
	script.async = true
	/* if (path === H_PATH) {
		script.id = "hs-script-loader"
		script.defer = true
	} */
	script.onload = () => {
		chatLoad = true
		if (path === H_PATH) {
			chatType = true
		}
	}
	script.onerror = () => {
		path !== H_PATH && loadScript(H_PATH)
	}
	script.src = path;
	document.getElementsByTagName("head")[0].appendChild(script);
}
loadScript(J_PATH);