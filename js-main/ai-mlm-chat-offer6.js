const upHeader = document.querySelector('.ai-mlm-upheader')
const header = document.querySelector('header.header')

const checkScrollToHideUpheader = () => {
    if(window.scrollY > upHeader.clientHeight){
        header.style.position = 'fixed'
    } else {
        header.style.position = 'relative'
    }
}
window.addEventListener('scroll', checkScrollToHideUpheader)

window.addEventListener('resize', checkScrollToHideUpheader)

window.addEventListener('load', checkScrollToHideUpheader)